FROM python:3.7-slim

RUN groupadd -r -g 1000 csi \
  && useradd --no-log-init -r -g csi -u 1000 csi

COPY requirements.txt /requirements.txt

RUN python -m venv /venv \
  && . /venv/bin/activate \
  && pip install --no-cache-dir -r /requirements.txt

COPY --chown=csi:csi . /app/
WORKDIR /app

ENV PATH /venv/bin:$PATH

USER csi

CMD ["/app/elog-telegram-bot"]
